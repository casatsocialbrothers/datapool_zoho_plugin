<?php
// /*
// Plugin Name: Datapool Zoho Koppeling
// Description: Haalt data op uit Zoho
// Author: Social Brothers
// Version: 1.5

// */
if(!defined('ABSPATH'))
{
    exit;
}

require_once wp_normalize_path(ABSPATH).'wp-load.php';
require_once plugin_dir_path( __FILE__ ) . 'class/Zoho_getter.php';
require_once plugin_dir_path( __FILE__ ) . 'cron_functions.php';


//default data
function zoho_options_default() {
	return array(
		'zoho_email'   	=> 'Gebruikersnaam',
        'zoho_password'	=> 'Wachtwoord',
	);

}

require_once plugin_dir_path( __FILE__ ) . 'post_types/register_post_types.php';
add_action( 'init', 'zoho_register_posttype', 0 );
add_action( 'init', 'zoho_register_taxonomies', 0 );
add_action( 'init', 'zoho_add_custom_fields', 0 );


// add_action('init', 'make_posts');
wp_schedule_event(time(), 'hourly', 'make_posts');
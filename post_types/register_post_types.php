<?php
function zoho_register_posttype()
{
    // Our custom post type function
    $jobs = array(
            'labels' => array(
                'name' => __('Opdrachten', '_SBB'),
                'singular_name' => __('Opdracht', '_SBB')
            ),
            'menu_icon' => 'dashicons-welcome-learn-more',
            'menu_position' => 22,
            'rewrite' => array('slug' => 'vacatures','with_front' => false),
            'public' => true,
            'has_archive' => false,
            'supports' => array('title', 'editor', 'thumbnail'),
        );

    register_post_type('jobs', $jobs);

}
function zoho_register_taxonomies()
{
    $job_cats = array(
        'labels' => array(
            'name' => __('Categorieën', '_SBB'),
            'singular_name' => __('Categorie', '_SBB'),
        ),
        'public' => false,
        'show_ui' => true,
        'rewrite' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
    );

    register_taxonomy('job_cat', array('jobs'), $job_cats);

    $market = array(
        'labels' => array(
            'name' => __('Markten', '_SBB'),
            'singular_name' => __('Markt', '_SBB'),
        ),
        'public' => false,
        'show_ui' => true,
        'rewrite' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
    );
    register_taxonomy('market', array('jobs'), $market);

    $location = array(
        'labels' => array(
            'name' => __('Locaties', '_SBB'),
            'singular_name' => __('Locatie', '_SBB'),
        ),
        'public' => false,
        'show_ui' => true,
        'rewrite' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
    );

    register_taxonomy('location', array('jobs'), $location);


    $sector = array(
        'labels' => array(
            'name' => __('Sectors', '_SBB'),
            'singular_name' => __('Sector', '_SBB'),
        ),
        'public' => false,
        'show_ui' => true,
        'rewrite' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
    );

    register_taxonomy('sector', array('jobs'), $sector);
}

function zoho_add_custom_fields()
{

    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array(
            'key' => 'group_5f5f542c59322',
            'title' => 'zoho',
            'fields' => array(
                array(
                    'key' => 'field_5f5f545438af3',
                    'label' => 'Uitgelicht',
                    'name' => 'uitgelicht',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> 'On'
                    ),
                    'instructions' => 'Uitgelicht?',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),
                array(
                    'key' => 'field_5f5f554b38af4',
                    'label' => 'Rol',
                    'name' => 'rol',
                    'type' => 'text',
                    'instructions' => 'Hier komt de rol',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f556c38af5',
                    'label' => 'Type',
                    'name' => 'type',
                    'type' => 'text',
                    'instructions' => 'Hier komt het type',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f558438af6',
                    'label' => 'Locatie',
                    'name' => 'locatie',
                    'type' => 'text',
                    'instructions' => 'Hier komt de locatie',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f559238af7',
                    'label' => 'Start',
                    'name' => 'start',
                    'type' => 'text',
                    'instructions' => 'Hier komt de start datum',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f55c038af8',
                    'label' => 'Uren per week',
                    'name' => 'uren_per_week',
                    'type' => 'text',
                    'instructions' => 'Hier komen de uren per week',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f55d938af9',
                    'label' => 'Duur van inzet',
                    'name' => 'duur_van_inzet',
                    'type' => 'text',
                    'instructions' => 'Hier komt de duur van inzet',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f55eb38afa',
                    'label' => 'Tarief indicatie',
                    'name' => 'tarief_indicatie',
                    'type' => 'text',
                    'instructions' => 'Hier komt de tarief indicatie',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f565338afb',
                    'label' => 'Referentie nummer',
                    'name' => 'referentie_nummer',
                    'type' => 'text',
                    'instructions' => 'Hier komt het refenentie nummer',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'jobs',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            
        ));
        
        acf_add_local_field_group(array(
            'key' => 'group_655f542c59322',
            'title' => 'custom field 1',
            'fields' => array(
                // custom fields 1
                array(
                    'key' => 'field_5f1f560338afb',
                    'label' => 'Afbeelding',
                    'name' => 'afbeelding_cf1',
                    'type' => 'text',
                    'instructions' => 'Hier komt een url naar een afbeelding',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f560338aft',
                    'label' => 'Afbeelding ondertitel',
                    'name' => 'afbeelding_subtitle_cf1',
                    'type' => 'text',
                    'instructions' => 'Hier komt omschrijving van de afbeelding',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f560388afb',
                    'label' => 'Titel',
                    'name' => 'title_cf1',
                    'type' => 'text',
                    'instructions' => 'Hier komt een titel',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f530338bfb',
                    'label' => 'Tekst',
                    'name' => 'tekst_cf1',
                    'type' => 'text',
                    'instructions' => 'Hier komt tekst',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),

            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'jobs',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            
        ));

        acf_add_local_field_group(array(
            'key' => 'group_6857f542c59322',
            'title' => 'Contact info recruiter',
            'fields' => array(
                // custom fields 2
                array(
                    'key' => 'field_5f5f530398afb',
                    'label' => 'Telefoonnummer',
                    'name' => 'telefoonnummer_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt een telefoonnummer',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f530358afb',
                    'label' => 'Emailadres',
                    'name' => 'emailadres_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt een emailadres',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f34530338afb',
                    'label' => 'LinkedIn',
                    'name' => 'linkedin_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt een linkedin link',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f78530338afb',
                    'label' => 'Naam medewerker',
                    'name' => 'naam_medewerker_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt de naam van de medewerker',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f5f530338afb',
                    'label' => 'Functietitel medewerker',
                    'name' => 'functietitel_medewerker_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt de functietitel van de medewerker',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_4f5f530338afb',
                    'label' => 'Foto medewerker',
                    'name' => 'foto_medewerker_cf2',
                    'type' => 'text',
                    'instructions' => 'Hier komt een foto van de medewerker',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    ),
                ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'jobs',
                    ),
                ),
            ),
            'menu_order' => 2,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            
        ));

        acf_add_local_field_group(array(
            'key' => 'group_68578751c59322',
            'title' => 'Custom sectie 3',
            'fields' => array(
                // custom fields 3
                array(
                    'key' => 'field_5f5f530338cs3',
                    'label' => 'Titel',
                    'name' => 'title_cf3',
                    'type' => 'text',
                    'instructions' => 'Hier komt een titel',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5f57540358afb',
                    'label' => 'Tekst',
                    'name' => 'tekst_cf3',
                    'type' => 'text',
                    'instructions' => 'Hier komt een tekst',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),

            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'jobs',
                    ),
                ),
            ),
            'menu_order' => 3,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            
        ));
    endif;
}
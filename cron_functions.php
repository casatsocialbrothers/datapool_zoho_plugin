<?php

function update_posts($data)
{
    $all_post_ids = get_posts(array(
        // 'fields'          => 'ids',
        'posts_per_page'  => -1,
        'post_type' => 'jobs'
    ));

    //create a list of deleted_posts that are no longer in ZoHo
    //create a list of posts we don't already have in WP.
    $deleted_post_ids=$all_post_ids;
    foreach($all_post_ids as  $key=> $post)
    {
        foreach($data['jobs'] as $index => $vac)
        {
            if(isset($vac['Posting Title']))
            {
                if($vac['Posting Title']==$post->post_title)
                {
                    $deleted_post_ids[$key]=0;
                    $data['jobs'][$index]=[];
                }
            }

        }
    }

    // delete posts from WP
    foreach($deleted_post_ids as $id)
    {
        if($id!=0){
            wp_delete_post($id->ID, true);
        }
    }

    // add new posts to WP
    foreach($data['jobs'] as $job)
    {
        $title="Title";
        if(isset($job['Posting Title']))
        {
            $title=$job['Posting Title'];
        }
        if(isset($job['JOBOPENINGID']))
        {
            $job_id=$job['JOBOPENINGID'];
        }
        $zoho_postarr = [
            'post_title'    => $title,
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type'     => 'jobs',
        ];
        if(isset($job['JOBOPENINGID'])){
            $id = wp_insert_post($zoho_postarr, true);
            zoho_add_post_meta($id, $job);        
            zoho_add_tax($id, $job);
        }
        
    }
}


//make posts
function make_posts()
{
    $zoho = new Zoho_getter(get_option('zoho_options')['username'],get_option('zoho_options')['password']);
    $data = $zoho->get();

    update_posts($data);

}

function zoho_add_tax($id, $job)
{
    
    //gets and formats location
    $job_cat    =   "";
    $market     =   "";
    $location   =   "";
    $sector     =   "";

    if(isset($job['Job Type']))
    {
        $job_cat=$job['Job Type'];
    }

    if(isset($job['Skillset']))
    {
        $market=$job['Skillset'];
    }

    if(isset($job['City']))
    {
        $location=$location. $job['City'];
    }

    if(isset($job['Industry']))
    {
        $sector=$job['Industry'];
    }

    $taxonomy = array (
        'job_cat'   =>  $job_cat,
        'market'    =>  $market,
        'location'  =>  $location,
        'sector'    =>  $sector
    );

    foreach($taxonomy as $key=>$value)
    {
        wp_set_object_terms($id,$value,$key);
    }
}

function zoho_add_post_meta($id, $job){

    $uitgelicht         ="";
    $rol                ="";
    $type               ="";
    $location           ="";
    $start              ="";
    $uren_per_week      ="";
    $duur_van_inzet     ="";
    $tarief_indicatie   ="";
    $referentie_nummer  ="";

    // custom field 1
    $afbeelding_url     ="";
    $afbeelding_subtitle="";
    $title_section_1    ="";
    $tekst_section_1    ="";

    // custom field 2 / contact info
    $telefoonnummer_cf2 = "";
    $emailadress_cf2    = "";
    $linkedin_cf2       = "";
    $naam_medewerker_cf2= "";
    $functietitel_medewerker_cf2    = "";
    $foto_medewerker = "";

    // custom field 3
    $title_section_3    ="";
    $tekst_section_3    ="";

    if(isset($job['Job Description']))
    {  
        $uitgelicht=$job['Job Description'];
    }

    if(isset($job['Skillset']))
    {  
        $rol=$job['Skillset'];
    }

    if(isset($job['Job Type']))
    {  
        $type=$job['Job Type'];
    }

    if(isset($job['Country']))
    {
        $location=$location. $job['Country'];
    }

    if(isset($job['State']))
    {
        if($location!=""){
            $location=$location.", ";
        }
        $location=$location. $job['State'];
    }

    if(isset($job['City']))
    {
        if($location!=""){
            $location=$location.", ";
        }
        $location=$location. $job['City'];
    }

    if(isset($job['Start']))
    {  
        $start=$job['Start'];
    }

    if(isset($job['Aantal uur']))
    {  
        $uren_per_week=$job['Aantal uur'];
    }

    if(isset($job['Duur'])&&isset($job['Duur eenheid']))
    {  
        $duur_van_inzet=$job['Duur']." ".$job['Duur eenheid'];
    }

    if(isset($job['Salary']))
    {  
        $tarief_indicatie=$job['Salary'];
    }

    if(isset($job['JOBOPENINGID']))
    {  
        $referentie_nummer=$job['JOBOPENINGID'];
    }

    // custom field 1
    if(isset($job['Afbeelding URL']))
    {  
        $afbeelding_url=$job['Afbeelding URL'];
    }

    if(isset($job['Ondertitel']))
    {  
        $afbeelding_subtitle=$job['Ondertitel'];
    }

    
    if(isset($job['Titel sectie 1']))
    {  
        $title_section_1=$job['Titel sectie 1'];
    }

    if(isset($job['Tekst sectie 1']))
    {  
        $tekst_section_1=$job['Tekst sectie 1'];
    }

    if(isset($job['Titel sectie 2']))
    {  
        $title_section_3=$job['Titel sectie 2'];
    }

    if(isset($job['Tekst sectie 2']))
    {  
        $tekst_section_3=$job['Tekst sectie 2'];
    }

    // custom field 2 / contact info
    if(isset($job['contact_data']['Mobile']))
    {
        $telefoonnummer_cf2 = $job['contact_data']['Mobile'];
    }

    if(isset($job['contact_data']['Email']))
    {
        $emailadress_cf2 = $job['contact_data']['Email'];
    }

    if(isset($job['contact_data']['LinkedIn']))
    {
        $linkedin_cf2 = $job['contact_data']['LinkedIn'];
    }

    if(isset($job['contact_data']['First Name'])&&isset($job['contact_data']['Last Name']))
    {
        $naam_medewerker_cf2=$job['contact_data']['First Name']." ".$job['contact_data']['Last Name'];
    }

    if(isset($job['contact_data']['Job Title']))
    {
        $functietitel_medewerker_cf2 = $job['contact_data']['Job Title'];
    }

    if(isset($job['contact_data']['Foto']))
    {
        $foto_medewerker = $job['contact_data']['Foto'];
    }

    add_post_meta($id, 'uitgelicht', $uitgelicht);
    add_post_meta($id, 'rol', $rol);
    add_post_meta($id, 'type', $type);
    add_post_meta($id, 'locatie', $location);
    add_post_meta($id, 'start', $start);
    add_post_meta($id, 'uren_per_week', $uren_per_week);
    add_post_meta($id, 'duur_van_inzet', $duur_van_inzet);
    add_post_meta($id, 'tarief_indicatie', $tarief_indicatie);
    add_post_meta($id, 'referentie_nummer', $referentie_nummer);
    
    // custom field 1
    add_post_meta($id, 'afbeelding_cf1', $afbeelding_url);
    add_post_meta($id, 'afbeelding_subtitle_cf1', $afbeelding_subtitle);
    add_post_meta($id, 'title_cf1', $title_section_1);
    add_post_meta($id, 'tekst_cf1', $tekst_section_1);

    // custom field 2 contact info
    add_post_meta($id, 'telefoonnummer_cf2', $telefoonnummer_cf2);
    add_post_meta($id, 'emailadres_cf2', $emailadress_cf2);
    add_post_meta($id, 'linkedin_cf2', $linkedin_cf2);
    add_post_meta($id, 'naam_medewerker_cf2', $naam_medewerker_cf2);
    add_post_meta($id, 'functietitel_medewerker_cf2', $functietitel_medewerker_cf2);
    add_post_meta($id, 'foto_medewerker_cf2', $foto_medewerker);

    // custom field 3
    add_post_meta($id, 'title_cf3', $title_section_3);
    add_post_meta($id, 'tekst_cf3', $tekst_section_3);

}
<?php

if(!defined('ABSPATH'))
{
    exit;
}

class Zoho_getter{
    private $email;
    private $password;
    private $token;

    function __construct($email, $password)
    {
        $this->email        = $email;
        $this->password     = $password;
        $this->token        = 'a76d7669bc429d051a9e9167fe74fc34';

    }


    private function api_call($url, $data, $post=1)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, $post);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
       
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);

        return $data;
    }

    // gets all contacts and returns it
    private function get_contacts()
    {
        $results = $this->api_call('https://recruit.zoho.eu/recruit/private/json/Contacts/getRecords',
            [
                'authtoken' => $this->token,
                'scope' => 'recruitapi',
                'version'=>2
            ]
        );
        $results = json_decode($results, true);

        if($this->validate_response($results)){   
            foreach($results['response']['result']['Contacts']['row'] as $row)
            {
                foreach($row["FL"] as $item)
                {
                    $user[$item['val']]=$item['content'];
                }
                if(isset($user['CONTACTID']))
                {
                    $users[$user['CONTACTID']]=$user;
                }
            }
            return $users;
        }
    }

    //creates auth token
    public function auth()
    {
        $nasty_response = $this->api_call(
            'https://accounts.zoho.eu/apiauthtoken/nb/create', 
            [
                'SCOPE' => 'ZohoRecruit/recruitapi',
                'EMAIL_ID' => $this->email,
                'PASSWORD'=> $this->password
            ]
        );

        //turn each line into its own string
        $nasty_response = explode($nasty_response[1],$nasty_response);

        $dirty_response=[];

        //get remove trash
        foreach($nasty_response as $line)
        {
            if(!strpos($line, '#')&&$line!='#')
            {
                $dirty_response[]=$line;
            }
        }
        //turn lines into a dictionairy
        foreach($dirty_response as $line)
        {
            $bits=explode("=", $line);
            if(isset($bits[1])){
                $response[$bits[0]]=$bits[1];
            }
        }

        if(isset($response['AUTHTOKEN']))
        {
            return ['result'=>true, 'data'=>$response['AUTHTOKEN']];
        }
        return ['result'=>false];
        
    }

    //checks the response for errors
    private function validate_response($response)
    {
        if(isset($response['response']['error']))
        {
            return false;
        }
        return true;
    }

    //gets all job offers
    public function get()
    {
        $dirty_response =  $this->api_call(
            'https://recruit.zoho.eu/recruit/private/json/JobOpenings/getRecords', 
            [
                'authtoken' => $this->token,
                'scope' => 'recruitapi',
                'version'=>2
            ]
        );
        $dirty_response = json_decode($dirty_response, true);
        $response = [];

        $contacts = $this->get_contacts();

        $vac_number=0;
        if($this->validate_response($dirty_response)){   
            foreach($dirty_response['response']['result']['JobOpenings']['row'] as $row)
            {
                foreach($row["FL"] as $item)
                {
                    // Replaces all html tags with a space :)
                    $info[$item['val']]=strip_tags(str_replace('<',' <',$item['content']));
                }
                if(isset($info['Contactpersoon_ID'])&&isset($contacts[$info['Contactpersoon_ID']]))
                {
                    $info['contact_data'] = $contacts[$info['Contactpersoon_ID']];
                }
                $response[]=$info;
                $vac_number++;
            }
            return ['count'=>$vac_number, 'jobs'=>$response];
        }
    }
}